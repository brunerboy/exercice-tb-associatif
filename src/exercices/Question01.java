package exercices;


import donnees.Club;
import donnees.Personne;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Question01 {

  public static void main(String[] args) {
      
      Map<String,Integer> repartVille= new TreeMap(); 
     
      System.out.println("\nNombre de personnes par villes\n");
   
  
  
      // A completer
     for(Personne pers : Club.listeDesPersonnes){
        int nbreP;
        
     if(repartVille.containsKey(pers.ville)) {
         nbreP= repartVille.get(pers.ville)+1;
     }
     else{
            nbreP=1;
     }
  repartVille.put(pers.ville,nbreP);
     }
    for(Entry ent : repartVille.entrySet()){
        System.out.println(ent.getKey()+ " " + ent.getValue());
    
    }
  
  }
}



