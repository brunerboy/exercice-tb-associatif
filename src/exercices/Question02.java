package exercices;


import donnees.Club;
import donnees.Personne;
import java.util.Map;
import java.util.TreeMap; 
import static utilitaires.UtilDojo.determineCategorie;

public class Question02 {

  public static void main(String[] args) {
      
      Map<String,Integer> repartCagtegPoids= new TreeMap(); 
     
      
      System.out.println("\nNombre de personnes par catégories de poids\n");

      
      // A completer
     for(Personne pers : Club.listeDesPersonnes){
    int n;
      String categ=determineCategorie(pers.sexe, pers.poids);
     if(repartCagtegPoids.containsKey(categ)){
         
       n = repartCagtegPoids.get(categ)+1;
     }
     else{
         n =1;
     }
     repartCagtegPoids.put(categ, n);
           } 
     
     for(Map.Entry ent : repartCagtegPoids.entrySet() ){
         System.out.printf("%-25s %-2s \n", ent.getKey(), ent.getValue());
     
     }
     
  
     
  
      
      
  }

  
}


